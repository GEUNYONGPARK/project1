void clearModel(int[][][] m) 
{
  for(int z = 0; z < mh; z++) {
    for(int x = 0; x < mw; x++) {
      for(int y = 0; y < md; y++) {
        m[z][x][y] = -1; // no cube
      }
    }
  }
}


void positionBrick(int[][][] model, color[] palette, float rot) 
{ 
  noStroke();
  pushMatrix();
  translate(width / 4, height / 2);
  rotateX(HALF_PI / 2);
  rotateY(rot);
  for(int h = 0; h < model.length; h++) {
    for(int x = 0; x < model[h].length; x++) {
      for(int y = 0; y < model[h][x].length; y++) { 
        int c = model[h][x][y];
        if (c == -1) continue;
      
        scale(5);
        translate(x * 5 - 20, y * 5 - 20, h * 5);

        fill(palette[c]);
        box(4.8);
        
        translate(-x * 5 + 20, -y * 5 + 20, -h * 5);
        scale(0.2);
      }
    }
  }
  popMatrix();
}


int[][][] setCSV(Table t) 
{
  int[][][] m = new int[mh][mw][md];
  clearModel(m);
  
  for (TableRow r : t.rows()) {
    int x = r.getInt("x");
    int y = r.getInt("y");
    int z = r.getInt("z");
    int c = r.getInt("c");
    m[z][x][y] = c;
  }
  return m;
}


Table getCSV(int[][][] model) 
{
  Table t = new Table();
  
  t.addColumn("x");
  t.addColumn("y");
  t.addColumn("z");
  t.addColumn("c");

    
  for(int h=0; h < model.length; h++) {
    for(int x=0; x < model[h].length; x++) {
      for(int y=0; y < model[h][x].length; y++) { 
        int c = model[h][x][y];
        if (c != -1) {
          TableRow r = t.addRow();
          r.setInt("x", x);
          r.setInt("y", y);
          r.setInt("z", h);
          r.setInt("c", c);
        }
      }
    }
  }
  return t;
}



