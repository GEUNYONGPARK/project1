# Customizable Spaceship Shooting Game #

It's a shooting game that you can build and play your own spaceship. 
You can customize your spaceship and fight againt three types of enemies.
In processing, you can't see your ranking and share you spaceship with other.
But by using the p5.js I try to impreve that features.


## Inspiration
---
I was inspired by the movie Star Wars. The focus was to build your own spacecraft so that you can enjoy games and share them with others.

## Table of content
---

* [Source code](https://bitbucket.org/GEUNYONGPARK/project1/src/master/Project1_Code/)
* [Demo video](https://www.youtube.com/watch?v=nPdquIfuM_M&feature=youtu.be) 
* Description of projects and notes in README.md (this file). 

## HOW TO PLAY
---
- Customize your own spaceship by clicking the box and add color. 
    1. With slider you can choose the layer. This game provide three layers. 
    2. Choose the color you want to draw. 
    3. Click and Drag the grids, you can see your own spaceship!! 
    4. After customizing your spaceship, please click submit and make sure you like it. 
    5. All done! Press the start button and enjoy it~


2. Play like shooting game, You need to avoid Enemies 
    1. There are three types of enemy. 
    2. When you shoot the enemies, you can get 10 scores. 
    3. When colliding with an enemy, the life is reduced by 10, and the game over when it reaches 0.

    ** Cotrol Keys ** 
    `←` : Go Left 
    `→` : Go Right 
    `↑` : Go Forward 
    `↓` : Go Backward 
    `Space Bar` : Shoot the Bullet


3. Check your scores
    I am gonna improve this part with p5.js.
    Such as sharing or rainking feature.

Build your favorite spaceship and get the best score.
Enjoy your flight~


## LIBRARY
---
- controlP5
    I use this for making GUI. Make RadioButton, Button, Slider.

- processing.sound
    My project is game! So I try to add sound with this library.



## SOURCES
---
I expanded the code that I made in __URP activities__ for a while and referred to the shooting game that we made at __Homework4__. And, terrain background came from shiffman's course. I take
his course and applying that code in my game. I fixed that to make it suitable for my game.



## STRUCTURE
---
My project consist of 3 Phases
`Customizing Spaceship Phase`, `Playing Game Phase`, `Score Phase`

To be specific, I divides as 7 tabs contain 3 classes. 
* Main code `Project1_Code` 
* `CreatCube` 
* `Background`
* `GUI`
* class `Spaceship` 
* class `Bullet` 
* class `Enemy`




### Project1_Code
This folder is main shows 3 phases.
Now I used phases to change the pages. But, I try to change it with state machine.

This phase consist of `setup()` and `draw()` to learn the entire game.


| Function name        | Description                                                          |
| ---                  | ---                                                                  |
| `fillLayer()`        | fill the grid by detecting the position with `mouseDragged()` and  `mousePressed()`
| `drawLayer()`        | Draw the grid that you can click or drag to draw your spaceship.     |



### CreatCube
This folder's main feature is

| Function name        | Description                                                          |
| ---                  | ---                                                                  |
| `clearModel()`       | It just clear the model                                              |
| `positionBrick()`    | It positioning the 3D cube on the right side of phase 1              |
| `setCSV()`           | It can save the data of spaceship as csv file                        |
| `getCSV()`           | It can load the csv data file                                        |



### Background
I think this code is not that meaningful. But, I really want to use it. Because its visual makes my games more active. 

| Function name        | Description                                                          |
| ---                  | ---                                                                  |
| `drawTerrain()`      | It draws terrain under my game.                                      |



### GUI
It contains every controller used in game.

| Function name        | Description                                                          |
| ---                  | ---                                                                  |
| `setupGui()`         | Draw every controller that used for game control Such as RadioButton, Button, Slider, testFilled.         |
| `cp5.addRadioButton()`| Make color selecting button                                         |
| `slider()`           | Control the layer of grid                                            |
| `remove()`           | Remove the color from grid                                           |
| `SUBMIT()`           | Make model rotate and save model as csv file                         |
| `START()`            | Load saved model and play the shooting game                          |
| `paletteColorchip()` | Show the color of each palette                                       |



### class Spaceship
Our main feature in this game. 
This class is load the 'spaceship.csv' file and play on the game.
Such as control the spaceship and check the collision between enemies.

| Function name        | Description                                                          |
| ---                  | ---                                                                  |
| `draw()`             | Load the Spaceship and help to control it by using allow key         |
| `checkcoliision()`   | for every section in field, check if the section can make a new      |
| `loadBrick()`        | It load the spaceship csv file                                       |


### class Bullet
It makes bullets that spaceship can shoot toward the enemies

| Function name        | Description                                                          |
| ---                  | ---                                                                  |
| `draw()`             | It draws the bullet and make it move toward the enemies              |
| `countBullet()`      | It counts the current bullet. if bullets exceed limit, it eliminates past bullets      |

### Enemy
It generates 3 types of enemies randomly. In this part I used __factory - design pattern__ for making diverse enemies.

| Function name        | Description                                                          |
| ---                  | ---                                                                  |
| `update()`           | It updates acc, vel, pos of enemies                                  |
| `draw()`             | This function produces the most basic form of enemy. This enemy is a one-ball.         |
| `checkBomb()`        | It detedct that enemies hit by bullets                               |
| `EnemyFactory()`     | It generates another types of enemies that comes from single ball basic enemies. enemy1 is two ball and enemy2 is 4 ball monsters                                     |


---
Thank you for reading me.
I forgot the pull with Gihub Desktop Sorry for updating it late.
But I submitted other things in time. Thank you again.


